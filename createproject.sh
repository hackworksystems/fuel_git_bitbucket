#!/bin/bash

#新規プロジェクトの作成
oil create $1
cd $1
rm -rf .git .gitmodules *.md docs fuel/core fuel/packages
git init
git submodule add git://github.com/fuel/core.git fuel/core
git submodule add git://github.com/fuel/oil.git fuel/packages/oil
git submodule add git://github.com/fuel/auth.git fuel/packages/auth
git submodule add git://github.com/fuel/parser.git fuel/packages/parser
git submodule add git://github.com/fuel/orm.git fuel/packages/orm
git submodule add git://github.com/fuel/email.git fuel/packages/email
git submodule add git://github.com/fuel/docs.git docs
git add .
git commit -m 'create project.'

# Bitbucket リポジトリの作成
read -p "Do you want to create Bitbucket Repository (y/n)? " is_create
if test $is_create = 'y' ; then
  read -p "Bitbucket Username: " username
  tty -s && echo
  read -p "Bitbucket ID(E-mail): " id
  tty -s && echo
  read -s -p "Bitbucket Password: " pass
  tty -s && echo
  api_result=`curl --request POST --user $id:$pass https://api.bitbucket.org/1.0/repositories/ --data name=$1 --data scm=git&is_private=True`
  echo $api_result
  if echo $api_result | grep 'create' ; then
    git remote add origin git@bitbucket.org:$username/$1.git
    git push origin master
  else
    echo 'Error: create repository'
  fi
fi

